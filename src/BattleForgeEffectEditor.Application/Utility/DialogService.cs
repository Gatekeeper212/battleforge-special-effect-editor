﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using Microsoft.Win32;
using System.Windows;
using System.Windows.Media;

namespace BattleForgeEffectEditor.Application.Utility
{
    static class DialogService
    {
        public static string SaveAsDialog(string title, string typeFilter, string defaultType)
        {
            SaveFileDialog fileDialog = new SaveFileDialog
            {
                Title = title,
                Filter = typeFilter,
                AddExtension = true,
                DefaultExt = defaultType
            };
            bool? result = fileDialog.ShowDialog();
            if (result == null || result == false)
                return string.Empty;
            return fileDialog.FileName;
        }

        public static string OpenFileDialog(string title, string typeFilter)
        {
            OpenFileDialog fileDialog = new OpenFileDialog
            {
                Title = title,
                Filter = typeFilter
            };
            bool? result = fileDialog.ShowDialog();
            if (result == null || result == false)
                return string.Empty;
            return fileDialog.FileName;
        }

        public static string OpenDirectoryDialog(string title)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                Description = title
            };
            System.Windows.Forms.DialogResult result = folderBrowserDialog.ShowDialog();
            return result == System.Windows.Forms.DialogResult.OK ? folderBrowserDialog.SelectedPath : string.Empty;
        }

        public static void ShowError(string caption, string text)
        {
            _ = MessageBox.Show(text, caption, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void ShowOK(string caption, string text)
        {
            _ = MessageBox.Show(text, caption, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static bool ShowAreYouSure(string caption, string text)
        {
            MessageBoxResult result = MessageBox.Show(text, caption, MessageBoxButton.YesNo, MessageBoxImage.Question);
            return result == MessageBoxResult.Yes;
        }

        public static Color ShowColorDialog(Color currentColor)
        {
            var colorDialog = new System.Windows.Forms.ColorDialog()
            {
                Color = System.Drawing.Color.FromArgb(currentColor.A, currentColor.R, currentColor.G, currentColor.B)
            };

            if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var resultColor = colorDialog.Color;
                return Color.FromArgb(resultColor.A, resultColor.R, resultColor.G, resultColor.B);
            }

            return currentColor;
        }
    }
}
