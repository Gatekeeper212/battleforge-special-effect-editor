﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using System;
using System.Globalization;
using System.IO;

namespace BattleForgeEffectEditor.Application.Utility
{
    public static class PathUtilities
    {
        /// <summary>
        /// Generates a backup file name from provided backup directory and original file name.
        /// </summary>
        /// <param name="backupDirectory">The directory where backup files are stored</param>
        /// <param name="originalFileName">The original file name</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown when either backup directory
        /// or original filename are empty or null</exception>
        public static string GetBackupFileName(string backupDirectory, string originalFileName)
        {
            if (string.IsNullOrEmpty(originalFileName))
                throw new ArgumentNullException(nameof(originalFileName));

            if (string.IsNullOrEmpty(backupDirectory))
                throw new ArgumentNullException(nameof(backupDirectory));

            string saveFileName = Path.GetFileNameWithoutExtension(originalFileName) + "-backup-"
                + DateTime.Now.ToString("MM-dd-yyyy-HH-mm-ss", CultureInfo.InvariantCulture)
                + Path.GetExtension(originalFileName);

            return Path.Combine(backupDirectory, saveFileName);
        }
    }
}
