﻿// BattleForge Special Effect Editor
// Copyright(C) 2023 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using BattleForgeEffectEditor.Application.Commands;
using BattleForgeEffectEditor.Application.Utility;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace BattleForgeEffectEditor.Application.View.GenericControls
{
    /// <summary>
    /// Interaction logic for TextureView.xaml
    /// </summary>
    public partial class TextureView : UserControl
    {
        public TextureView()
        {
            InitializeComponent();
        }

        public Color BackgroundColor
        {
            get => (Color)GetValue(BackgroundColorProperty);
            set => SetValue(BackgroundColorProperty, value);
        }

        [Description("Background color for the texture"), Category("Texture")]
        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register(nameof(BackgroundColor), typeof(Color), typeof(TextureView), new PropertyMetadata(Colors.Transparent));

        public string TextureFilePath
        {
            get => (string)GetValue(TextureFilePathProperty);
            set => SetValue(TextureFilePathProperty, value);
        }

        [Description("File path of the texture"), Category("Texture")]
        public static readonly DependencyProperty TextureFilePathProperty =
            DependencyProperty.Register(nameof(TextureFilePath), typeof(string), typeof(TextureView), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register(nameof(Title), typeof(string), typeof(TextureView), new PropertyMetadata("Texture"));

        public ICommand WhiteColorCommand => new RelayCommand((_) => BackgroundColor = Colors.White);
        public ICommand BlackColorCommand => new RelayCommand((_) => BackgroundColor = Colors.Black);
        public ICommand ChooseColorCommand => new RelayCommand((_) => OnChooseColorClick());

        private void OnChooseColorClick()
        {
            BackgroundColor = DialogService.ShowColorDialog(BackgroundColor);
        }
    }
}